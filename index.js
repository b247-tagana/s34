// 1. Import express using require directive
const express = require("express");

// 2. Initialize express by using it to the app variable as a function
const app = express();

// 3. Set the port number
const port = 4004;

// 4. Use middleware to allow express to read JSON
app.use(express.json());

// 5. Use middleware to allow express to be able to read more data types from a response
app.use(express.urlencoded({extended: true}));

// 6. Listen to the port and console.log a text once the server is running
app.listen(port, () => console.log(`Server is running at localhost: ${port}`));

// [SECTION] Routes
	// Express has methods corresponding to each HTTP method

app.get("/hello", (request, response) => {
	response.send('Hello from the /hello endpoint')
});

app.post("/display-name", (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});

// [SUB-SECTION] Sign-Up Form
let users = [];

app.post("/sign-up", (req, res) => {
	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`);
	} else {
		res.send("Please input BOTH username and password!");
	}
});

app.put("/change-password", (req, res) => {
	// Creates a variable to store the message to be sent back to the client/Postman
	let message;

	// Creates a for loop that will loop through the elements of the "users" array
	for(let i = 0; i < users.length; i++){

		// If the username provided in the client/Postman and the username of the current object in the loop is the same
		if(req.body.username == users[i].username){

			// Changes the password of the user found by the loop into the password provided in the client/Postman
			users[i].password = req.body.password;

			// Changes the message to be sent back by the response
			message = `User ${req.body.username}'s password has been updated.`;

			// Breaks out of the loop once a user that matches the username provided in the client/Postman is found
			break;

		// If no user was found
		} else {

			// Changes the message to be sent back by the response
			message = "User does not exist";
		}
	}

	// Send a response back to the client/Postman once the password has been updated or if a user is not found
	res.send(message);
})

// [ACTIVITY]

// 1. Create a GET route that will access the /home route that will print out a simple message.
// 2. Process a GET request at the /home route using postman.

app.get("/home", (request, response) => {
	response.send('Welcome to the home page')
});

// 3. Create a GET route that will access the /users route that will retrieve all the users in the mock database.
// 4. Process a GET request at the /users route using postman.

let usersAccount = [
		{
			"username" : "johndoe",
			"password" : "johndoe1234"
		}
	]

app.get("/users", (request, response) => {
	response.send(usersAccount);
});

// 5. Create a DELETE route that will access the /delete-user route to remove a user from the mock database.
// 6. Process a DELETE request at the /delete-user route using postman.

app.delete("/delete-user", (request, response) => {
	response.send(`User ${request.body.username} has been deleted.`);
});